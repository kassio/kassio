+++

+++

# Ideas

Some ideas that _should_ became blog posts

## When a command needs a file as argument

Some commands/scripts might need a file as argument, but you might want to `echo` something instead.

Simple solution I found, instead of creating a file for a quick run:

```console
$ echo "Some thing that should be in a file" | command-that-requires-a-file /dev/stdin
```

Since `/dev/stdin` is a readable file, this should work in most cases.

## Ruby logic tables

### Logic `and`

```ruby
> [true, false].repeated_permutation(4).to_a.map { _1 << _1.reduce(:&) }
=> [[true, true, true, true, true],
 [true, true, true, false, false],
 [true, true, false, true, false],
 [true, true, false, false, false],
 [true, false, true, true, false],
 [true, false, true, false, false],
 [true, false, false, true, false],
 [true, false, false, false, false],
 [false, true, true, true, false],
 [false, true, true, false, false],
 [false, true, false, true, false],
 [false, true, false, false, false],
 [false, false, true, true, false],
 [false, false, true, false, false],
 [false, false, false, true, false],
 [false, false, false, false, false]]
```

### Logic `or`

```ruby
> [true, false].repeated_permutation(4).to_a.map { _1 << _1.reduce(:|) }
=> [[true, true, true, true, true],
 [true, true, true, false, true],
 [true, true, false, true, true],
 [true, true, false, false, true],
 [true, false, true, true, true],
 [true, false, true, false, true],
 [true, false, false, true, true],
 [true, false, false, false, true],
 [false, true, true, true, true],
 [false, true, true, false, true],
 [false, true, false, true, true],
 [false, true, false, false, true],
 [false, false, true, true, true],
 [false, false, true, false, true],
 [false, false, false, true, true],
 [false, false, false, false, false]]
```

## Git tricks

- Explore some useful git scripts/alias that I created over the years
  - `git exclude`
  - `git today`
  - `git branch-main`
  - `grt`
  - `nvim-diff`/`rspec-diff`/etc
- Explain about the `git-*` in the user `$PATH`
- Explore less known git features like:
  - `$GIT_PAGER`
  - Log single function (for some langs) changes from given date:
    - `git log --oneline -s -L:serveContent:src/net/http/fs.go --since="2021-02-16"`
  - Log between patterns from a file:
    - `git log -L "/def name_of_your_method/,/^\s*end/:lib/path/to/your/file.rb"`

## Code alignment

Keeping the code aligned to the left makes it easier to read (assuming that you
are programming in a language written from left to right). Since we read from
left to right having most of the code aligned to the left means that you have
more code visible.
[Reference](https://www.rahulpnath.com/blog/left-align-your-code-for-better-readability/)

Your code contains a hierarchy of elements, be it classes, methods, loops or
even nested loops. Each hierarchy has its own commands and components which are
not available for outer scopes. These blocks of code need to be in the same
vertical line or levels to see which variables belong to which block.
[Reference](https://www.codegrip.tech/productivity/how-to-format-code-for-maximum-code-readability/)

## Code is communication

- Like public communication
  - we should have the same concerns of a public speach
- Wiios laws in coding
- Empathy
  - we need empathy with the people and the _state_ when the code was created
- Other communication treats

---

## Proud gambiarra

- Explore how I script local/personal solutions for my day-to-day coding and debugging.
- Explain how this like `upgrade` and `git exclude` are powerful tools to keep local tooling
