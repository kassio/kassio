---
title: "Retrying commands"
date: 2024-12-02T10:46:01Z
draft: true
---

Sometimes we need to retry commands, flaky-test, testing network or something like that. For these
moments I started using these scripts to help me.

## Until it fails

```shell
#!/usr/bin/env zsh

count=100
case "${1}" in
  -h|--help)
    echo "Usage: until-fail [-n|--limit] <cmd>

    -n | --limit number of retries
    "
    exit 0
    ;;
  -n|--count)
    shift
    count=${1}
    shift
    ;;
esac

current=0
while [ "${current}" -lt "${count}" ] && "${@}" >/dev/null; do
  clear
  current=$((current + 1))
  echo "» retry ${current}/${count}: ${*}"
done
```

#### Usage:

```shell
$ until-fail bundle exec rspec specs/models/user_spec.rb:35

# ... running output

» retry 1/100: bundle exec rspec specs/models/user_spec.rb:35

# ... running output
```

## Until it succeed

```shell
#!/usr/bin/env zsh

set -x
count=100
case "${1}" in
  -h|--help)
    echo "Usage: until-success [-n|--limit] <cmd>

    -n | --limit number of retries
    "
    exit 0
    ;;
  -n|--count)
    shift
    count=${1}
    shift
    ;;
esac

current=0
while [ "${current}" -lt "${count}" ] && ! "${@}" >/dev/null; do
  clear
  current=$((current + 1))
  echo "» retry ${current}/${count}: ${*}"
done
```

#### Usage:

```shell
$ until-succeed ping google.com

# ... running output

» retry 1/100: ping google.com

# ... running output
```
