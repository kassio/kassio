+++
date = ''
title = 'Proud gambiarra'
aliases = ['/drafts/proud-gambiarra.html']
draft = true
+++

## What's gambiarra?

> A brazilian expression. It basically means to use improvised methods/solutions to solve a problem,
> with any avaiable material.

- [UrbanDictionary](https://www.urbandictionary.com/define.php?term=Gambiarra)

Usually, the term is associated to _unexpected_ solutions that are usually
_weak_ or _not safe_, like:

{{< centralized_image
  src="/posts/2022/assets/gambiarra.png"
  alt="gambiarra" >}}

But in the context of this post, gambiarra will be a set of techniques, scripts or practices that I
do that sometimes surprises my pears.

## Why am I proud of my gambiarra?

Simply because I think they help my day-to-day work, and sometimes they are fun. :smiley:

## How do I do it?

### The bases

If I had to resume I would say:

- documentation (not necessary text)
  - documentation might be any kind of information that make some concept/idea easier to understand.
  In a lot of times, for me, I document _stuff_ by abstracting an concept/idea in a script or alias.
- git
  - I like to ensure that my gambiarra will be part of my utility belt, therefore I carry it
  everywhere - I mean, in the cloud :cloud:. Also, this make it easier when changing computers.
- script (bash, zsh, ruby or more recently some little go)
  - It's just the way my mind works, but other people might use other _tools_ to achieve similar
  results.

### Ok, but gimme examples!

### Security

### Conclusion
