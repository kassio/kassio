+++
date = ''
title = 'Union Power'
aliases = ['/drafts/union-power.html']
draft = true
+++

I'm might be late for this (and or misguided), but, if the intention was to avoid a N+1 query while fetching cascaded settings, did you consider using sql UNION for that? We could use a UNION with a virtual priority field (1 for instance, 2 for group, 3 for project for example), and check for the setting with value on the highest priority. This should eliminate the N+1
and have a good performance with a CTE.
I didn't look into the db scheme we have in place for that, but I did this quick test with a fake db structure just to exemplify what I mean.
fake db structure and values just for testing

```sql
CREATE DATABASE cascading_test;
\c cascading_test;

DROP TABLE IF EXISTS instances;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS projects;

CREATE TABLE instances (setting text, value integer);
CREATE TABLE groups (setting text, value integer);
CREATE TABLE projects (setting text, value integer);

INSERT INTO instances (setting, value) VALUES ('A', 1);

INSERT INTO instances (setting, value) VALUES ('B', 1);
INSERT INTO groups (setting, value) VALUES ('B', 2);

INSERT INTO instances (setting, value) VALUES ('C', 1);
INSERT INTO groups (setting, value) VALUES ('C', 2);
INSERT INTO projects (setting, value) VALUES ('C', 3);
```

When only instance has value

```sql
cascading_test =# SELECT setting, value FROM (
 ( >   (SELECT 1 as priority, setting, value FROM instances)
 ( >   UNION
 ( >   (SELECT 2 as priority, setting, value FROM groups)
 ( >   UNION
 ( >   (SELECT 3 as priority, setting, value FROM projects)
 ( > ) SETTINGS
 - > WHERE setting = 'A'
 - > AND value IS NOT NULL
 - > ORDER BY priority desc
 - > LIMIT 1;
 setting | value
---------+-------
 A       |     1
 ```

When instance and group has value

```sql
SELECT setting, value FROM (
 ( >   (SELECT 1 as priority, setting, value FROM instances)
 ( >   UNION
 ( >   (SELECT 2 as priority, setting, value FROM groups)
 ( >   UNION
 ( >   (SELECT 3 as priority, setting, value FROM projects)
 ( > ) SETTINGS
 - > WHERE setting = 'B'
 - > AND value IS NOT NULL
 - > ORDER BY priority desc
 - > LIMIT 1;
 setting | value
---------+-------
 B       |     2
 ```

When project, group and instance has value

```sql
SELECT setting, value FROM (
 ( >   (SELECT 1 as priority, setting, value FROM instances)
 ( >   UNION
 ( >   (SELECT 2 as priority, setting, value FROM groups)
 ( >   UNION
 ( >   (SELECT 3 as priority, setting, value FROM projects)
 ( > ) SETTINGS
 - > WHERE setting = 'C'
 - > AND value IS NOT NULL
 - > ORDER BY priority desc
 - > LIMIT 1;
 setting | value
---------+-------
 C       |     3
```
