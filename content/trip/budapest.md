---
title: "Budapest"
draft: true
---

* Prédio - [Hungarian Parliament Building](https://www.google.com/url?q=https://maps.app.goo.gl/dGRWpG5956fUkhYk8&sa=D&source=editors&ust=1716907752004591&usg=AOvVaw1f1vvhkE3xij58doq8g0wj) 
* Estátua - [Shoes on the Danube Bank](https://www.google.com/url?q=https://maps.app.goo.gl/JE5jpbKBNLgCBr4T8&sa=D&source=editors&ust=1716907752005154&usg=AOvVaw1syWVtWayz3j8kyUK7XxVi)
* Prédio - [Fisherman's Bastion](https://www.google.com/url?q=https://maps.app.goo.gl/qgXgBYcanQ4r3BoGA&sa=D&source=editors&ust=1716907752005709&usg=AOvVaw3XHtsipfacsDBeGTSto0Aa)
* Prédio/Museu [Buda Castle](https://www.google.com/url?q=https://maps.app.goo.gl/Fcuh6htnWT1kk4gQ6&sa=D&source=editors&ust=1716907752006132&usg=AOvVaw0WdsH_35SnyB-qo5pS2Z8l)
* Parque - [Citadella](https://www.google.com/url?q=https://maps.app.goo.gl/2iPjCwe2gBdUXGEv8&sa=D&source=editors&ust=1716907752006480&usg=AOvVaw0qFUUKNsPaD5d_5d6AnpXu)
* Igreja - [Gellért Hill Cave](https://www.google.com/url?q=https://maps.app.goo.gl/vxtRNgQ9Si3wfQvH8&sa=D&source=editors&ust=1716907752006996&usg=AOvVaw2ulFfsttT-vvOgLnrDb3rq)
* Prédio - [Vajdahunyad Castle](https://www.google.com/url?q=https://maps.app.goo.gl/VVbt4zKAEsNke5PKA&sa=D&source=editors&ust=1716907752007435&usg=AOvVaw3QudQmPXFPoVbZxcK379Hf)
* Prédio - [Dohány Street Synagogue](https://www.google.com/url?q=https://maps.app.goo.gl/8sYK4SGfdmtc6ayEA&sa=D&source=editors&ust=1716907752007894&usg=AOvVaw3oBSIzBbYJ_Xj9rxLe0RbT)
* Piscinas públicas termais - [Széchenyi Thermal Bath](https://www.google.com/url?q=https://maps.app.goo.gl/aBTdDcwVfwKjtpFD8&sa=D&source=editors&ust=1716907752008368&usg=AOvVaw2iivtzJTI-shYp5bU0N-Vj)
