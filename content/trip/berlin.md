---
title: "Berlin"
draft: true
---

* Prédio - [Reichstag Building](https://www.google.com/url?q=https://maps.app.goo.gl/cFNMwKqBsuHe5NKf9&sa=D&source=editors&ust=1716907751988568&usg=AOvVaw3MZ2qhSevNfHKEPucLam5G) - Parlamento Alemão
* Passeio na cobertura do prédio e visita a Cúpula
  * [https://www.bundestag.de/pt](https://www.google.com/url?q=https://www.bundestag.de/pt&sa=D&source=editors&ust=1716907751988983&usg=AOvVaw1fysOT4ZlHWoxvEVFK7yVw)
* Monumento - [Brandenburg Gate](https://www.google.com/url?q=https://maps.app.goo.gl/FCTgsseVHDsirex19&sa=D&source=editors&ust=1716907751989337&usg=AOvVaw3fEJ2qXAblHsxx2c0eDeIk)
* [Memorial to the Murdered Jews of Europe](https://www.google.com/url?q=https://maps.app.goo.gl/dJ8NjBwss8Bg3vhbA&sa=D&source=editors&ust=1716907751989878&usg=AOvVaw3RFtnd33BQjDC7th5LeOpp)
* Tem um museu em baixo que é bem legal contando a história dos Judeus.
  * [https://www.stiftung-denkmal.de/](https://www.google.com/url?q=https://www.stiftung-denkmal.de/&sa=D&source=editors&ust=1716907751990474&usg=AOvVaw04NyMkJr0TnrK5Je4V76wT)
* [Museum Island](https://www.google.com/url?q=https://maps.app.goo.gl/QKXJyUVNjp9qxcSA6&sa=D&source=editors&ust=1716907751990967&usg=AOvVaw3HgtVLBCTDHEXZSQ8MGAeW)
* Uma ilhazinha cheia de museus, mesmo se não entrar nos museus vale a pena dar uma passeada por lá para ver os prédios que são muito bonitos
  * [Berlin Cathedral](https://www.google.com/url?q=https://maps.app.goo.gl/1jc3EfjxYALG626N6&sa=D&source=editors&ust=1716907751991479&usg=AOvVaw2VJUlG2GQQXpr2GL7NYLEL)
* Parece linda por dentro, mas paga para entrar.
  * [https://www.berlinerdom.de](https://www.google.com/url?q=https://www.berlinerdom.de&sa=D&source=editors&ust=1716907751992162&usg=AOvVaw2RJJbFmXJcn1uHySYf-hi4)
* [Berliner Fernsehturm](https://www.google.com/url?q=https://maps.app.goo.gl/LbyzWjfCJgVBgPP88&sa=D&source=editors&ust=1716907751992694&usg=AOvVaw2iqXUcnxQ4wHgtuKRtJ6z-) (Torre de TV)
* Existem várias opções de visita, mas acho que a mais "básica" para ver a cidade de cima é o suficiente. Fomos ao restaurante lá e não foi tão legal assim.
  * [https://tv-turm.de/](https://www.google.com/url?q=https://tv-turm.de/&sa=D&source=editors&ust=1716907751993478&usg=AOvVaw3I_rZ-2JTPJT8WmUCt5onq)
* [Anne Frank Center](https://www.google.com/url?q=https://maps.app.goo.gl/3zjfBfDRv5V5wjpb6&sa=D&source=editors&ust=1716907751993949&usg=AOvVaw299w7VAEfdRM_ahFfP19a2)
  * Não chegamos a entrar no centro, mas o beco é bem estiloso, cheio de arte. A região também é bem legal com outros becos cheios de lojinhas, cafés, sorveterias, etc.
* [East Side Gallery](https://www.google.com/url?q=https://maps.app.goo.gl/X9RNHWaaHz3E2Ewr5&sa=D&source=editors&ust=1716907751994595&usg=AOvVaw1vh_EN7R_iLcuDRf81AEF0)
  * Parte do muro no lado leste de Berlim. Hoje é uma grande galeria de arte a céu aberto. É uma região bem legal para passear tb.
* [Oberbaum Bridge](https://www.google.com/url?q=https://maps.app.goo.gl/iiZVNH1NRKvwzmRW6&sa=D&source=editors&ust=1716907751995255&usg=AOvVaw0G66r72PxQTlk5Ff7oJTVl)
  * Uma ponte estilosa e perto do East Side Gallery.
* [Kaiser Wilhelm Memorial Church](https://www.google.com/url?q=https://maps.app.goo.gl/cyPzyqeP9MH4dUTcA&sa=D&source=editors&ust=1716907751995762&usg=AOvVaw2VLSFGDFXDBhUXhvTI7PJs)
  * Ruínas de uma igreja destruída pela guerra, mas que teve algumas adaptações modernas.
  * Na mesma região tem o zoológico. Mesmo se não forem ao Zoo, tem o [Bikini Shopping Center](https://www.google.com/url?q=https://maps.app.goo.gl/mmrPvNmUDjQGs27o9&sa=D&source=editors&ust=1716907751996282&usg=AOvVaw2VO-cT2kIYXk9SEDP0cJzw) que é literalmente "colado" no Zoo, da praça de alimentação desse shopping center dá para ver os Gorilas.
