---
title: "Vienna"
draft: true
---

* Parque de diversões (entrada gratuita, e fica num parque, vale a pena o passeio) -[Viennese Giant Ferris Wheel](https://www.google.com/url?q=https://maps.app.goo.gl/hhgi1FsJyzYgwU5X8&sa=D&source=editors&ust=1716907752008910&usg=AOvVaw1zrlimjiAkUlWVAiZbzGct) - [https://wienerriesenrad.com/en/history/](https://www.google.com/url?q=https://wienerriesenrad.com/en/history/&sa=D&source=editors&ust=1716907752009167&usg=AOvVaw1OvL_xCDf5cR5wkEZFylrw)
* Ópera - [Vienna State Opera](https://www.google.com/url?q=https://maps.app.goo.gl/rPzDCMNP4SJAcviG8&sa=D&source=editors&ust=1716907752009658&usg=AOvVaw1XwaLyXNy5PKgjLAjvCzMj)
  * Se conseguir assistir uma ópera, super recomendo. Fomos ano passado e gostei demais da experiência. 🙂
* Igreja - [St. Charles's Church](https://www.google.com/url?q=https://maps.app.goo.gl/1qHhY67LHto8ddgEA&sa=D&source=editors&ust=1716907752010260&usg=AOvVaw3HQnFmyfH5HH-1P0bQv0-T)
* Igreja - [St. Stephen's Cathedral](https://www.google.com/url?q=https://maps.app.goo.gl/XavnE9KwzAKxpMDF7&sa=D&source=editors&ust=1716907752010634&usg=AOvVaw1_IKLOiXdOeOA4QtvfNpGZ)
* [Ankeruhr](https://www.google.com/url?q=https://maps.app.goo.gl/xhZcN6gGCvhmS8LSA&sa=D&source=editors&ust=1716907752011067&usg=AOvVaw3Mr4tzy6eIhNpfU5grK-hY) - relógio bonitinho na rua, o de praga é mais legal. 😬
* Tem uns museus que não entramos, mas a arquitetura externa deles já são lindas
* [Kunsthistorisches Museum Wien](https://www.google.com/url?q=https://maps.app.goo.gl/uH5rp45ZDiFgDzyP8&sa=D&source=editors&ust=1716907752011515&usg=AOvVaw01WTHlGHubckezYmK0b8rw)
* [Hofburg](https://www.google.com/url?q=https://maps.app.goo.gl/gqasaawp2ZFLSttf8&sa=D&source=editors&ust=1716907752011902&usg=AOvVaw3iWlTUkA-LzndSLVRyO1Wy)
* Predio - [Rathausplatz](https://www.google.com/url?q=https://maps.app.goo.gl/YmQoPxJPv8bi7gKJ6&sa=D&source=editors&ust=1716907752012339&usg=AOvVaw3DGeDaTkOwLLVgOTFrqw9k) - “prefeitura” - prédio bem bonito e às vezes tem uns eventos nos jardins.
* Teatro - [Burgtheater](https://www.google.com/url?q=https://maps.app.goo.gl/CxdR6G3QawnLdsAJ8&sa=D&source=editors&ust=1716907752012843&usg=AOvVaw3AdKOQZPnalS0bjQ8_Xdwy)
* Museu - [Sigmund Freud Museum](https://www.google.com/url?q=https://maps.app.goo.gl/VVLjdBnQKCNAeGE89&sa=D&source=editors&ust=1716907752013370&usg=AOvVaw33y9k7CVE-0j61vJdQ_EJK) - Casa  do Freud, não fomos mas dizem que é muito legal
* Bar - [Monte Ofelio](https://www.google.com/url?q=https://maps.app.goo.gl/fCEo8zsMaA9aW9Ty6&sa=D&source=editors&ust=1716907752013971&usg=AOvVaw3am4G9OgLB901NzW3WiTYU) - Bons drinks
* Bar - [Jazzland](https://www.google.com/url?q=https://maps.app.goo.gl/D25Ua7CenqWTPQ3CA&sa=D&source=editors&ust=1716907752014553&usg=AOvVaw0SauyGrGLI-jr1bJUS84ht) - Bar de jazz muito legal, no subterrâneo. (não aceita cartão, só dinheiro)
* Museu - [Wien Museum Mozart apartment](https://www.google.com/url?q=https://maps.app.goo.gl/SqsAckn8yDyboUw38&sa=D&source=editors&ust=1716907752015050&usg=AOvVaw2MmQYYfQA-C-kAnokjARAi) - Casa do Mozart - achei mais ou menos. Mas se gostam desse tipo de história pode valer a pena.
* Palácio - [Schönbrunn Palace](https://www.google.com/url?q=https://maps.app.goo.gl/tDtgJuVo2yMtaEgK7&sa=D&source=editors&ust=1716907752015477&usg=AOvVaw2BEOgznMbhARucksH3uTQW) - Palácio muito lindinho e gigante.
* Se tiverem tempo/animo de pegar um carro por uns dias super aconselho:
  * [Gmunden](https://www.google.com/url?q=https://maps.app.goo.gl/9QKdVxZydNagH4E77&sa=D&source=editors&ust=1716907752015903&usg=AOvVaw1zK022nfjLanocXT-arSoI) - cidadezinha na beira do lago muito lindinha que tem essa atração no topo da montanha: [Baumwipfelpfad Salzkammergut](https://www.google.com/url?q=https://maps.app.goo.gl/BRBiVsw62xt8BA6m8&sa=D&source=editors&ust=1716907752016249&usg=AOvVaw1bOGuCC9CPZ7eRac3riJSo)
  * [Hallstatt](https://www.google.com/url?q=https://maps.app.goo.gl/kYxWJmBXVgRz5cuL6&sa=D&source=editors&ust=1716907752016758&usg=AOvVaw31kZkIx61xkQ5ecFz9W7DR) - outra cidadezinha na beira de um lago, super famosinha nos instagrams, que tem uma visitação bem legal em uma mina de sal: [Salzwelten Hallstatt](https://www.google.com/url?q=https://maps.app.goo.gl/xDDkJ8tecbEyKcrF9&sa=D&source=editors&ust=1716907752017077&usg=AOvVaw3_2x-jidpFf5YDiVgDrfF1)
