---
title: "Munique"
draft: true
---

* Natureza - [Eisbachwelle](https://www.google.com/url?q=https://maps.app.goo.gl/mvuxSkKZJiUJkaM58&sa=D&source=editors&ust=1716907752017743&usg=AOvVaw0I0yWyItW_gQboqEkmZKjK)
  * Galera surfando no rio. Mandem fotos de vocês tentando!! Hahaah
* Prédio - [Maximilianeum](https://www.google.com/url?q=https://maps.app.goo.gl/S324ycBcQc26P45q8&sa=D&source=editors&ust=1716907752018194&usg=AOvVaw3av2f4_QeQIa2zjyiTTqQW)
* Igreja - [Asamkirche](https://www.google.com/url?q=https://maps.app.goo.gl/mj8WHQWHipCxQZkg7&sa=D&source=editors&ust=1716907752018548&usg=AOvVaw3FHekAkLbDQCADl1ezvSq-)
* Prédio - [Justizpalast München](https://www.google.com/url?q=https://maps.app.goo.gl/K9H4FJefyX37dR5v6&sa=D&source=editors&ust=1716907752018844&usg=AOvVaw1DDU2CH_e524OqDe_aaSmY)
* Praça - [Karlsplatz](https://www.google.com/url?q=https://maps.app.goo.gl/8LDHwEd6MaBQswPB8&sa=D&source=editors&ust=1716907752019270&usg=AOvVaw1W9BbhxTeADs1lXvRnpFuV)
* Restaurante - [Park Café](https://www.google.com/url?q=https://maps.app.goo.gl/7nVmbmE7LSMJyTBA8&sa=D&source=editors&ust=1716907752019634&usg=AOvVaw3y0lGSemtfVP2ug2fNrS36)
* Estátua - [Bavaria Statue](https://www.google.com/url?q=https://maps.app.goo.gl/cXojk28TftPa7QJQA&sa=D&source=editors&ust=1716907752020123&usg=AOvVaw2zAkMwcGqQ-JfehFwBjSs8)
* Praça principal - [Marienplatz](https://www.google.com/url?q=https://maps.app.goo.gl/smHyuN7kXGQeoMCB8&sa=D&source=editors&ust=1716907752020553&usg=AOvVaw3NZBDBnlsPKaQvAP6AzQeY)
* Parque - [Hofgarten](https://www.google.com/url?q=https://maps.app.goo.gl/Pf8XtTK8x5z3T3ga7&sa=D&source=editors&ust=1716907752021081&usg=AOvVaw2tYUmqnGv-hHTws56omQRm)
