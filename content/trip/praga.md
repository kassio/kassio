---
title: "Praga"
draft: true
---


* Praga é uma cidade muito linda com muita coisa para ver nas ruas. Vou colocar alguns pontos aqui que são “apenas” estátuas ou prédios bonitos.
* Algumas atrações:

* Só de andar na região central/turística, já é bem legal. São varias vielas/ruazinhas com muitos bares e restaurantes. No verão provavelmente vai estar um pouco mais que lotado. 😅
* Museu - [Národní Muzeum](https://www.google.com/url?q=https://maps.app.goo.gl/dudqMBekHaChNy6o8&sa=D&source=editors&ust=1716907751997189&usg=AOvVaw2AU4mCkFwY-jQ-9PG0lJQ1) - Muito lindo por dentro e por fora.

* Esse museu fica bem na região central, ali perto vão ter muitos bares, restaurantes, praças bem legais. Só tomem cuidado que tem uns “pega turista” por lá. 😉

* Estátua: [Hanging man sculpture](https://www.google.com/url?q=https://maps.app.goo.gl/1WqEAejv5ZhE1osCA&sa=D&source=editors&ust=1716907751997896&usg=AOvVaw21ZN5YHYmneznDNtWAPbe7)

* olhem para cima, ele realmente está pendurado

* Estátua: [Man Hanging Out - David Černý's Statue of Sigmund Freud](https://www.google.com/url?q=https://maps.app.goo.gl/9Pw9LgtRC62JhFF86&sa=D&source=editors&ust=1716907751998401&usg=AOvVaw1HIFse8n0Bvkug7dQ8_LTw)

* yeah, vocês entenderam né?! Olhem para cima! hahah

* Estátua: [Lilith by David Černý](https://www.google.com/url?q=https://maps.app.goo.gl/yzJd4uYr5sSzSLW36&sa=D&source=editors&ust=1716907751999014&usg=AOvVaw0OT0clpry7mfr2kWtZpJH4)

* se prestarem atenção vão perceber que ela mexe a cabeça
* próximo da Lilith tem outras obras do mesmo autor no mesmo estilo

* Prédio: [Dancing House](https://www.google.com/url?q=https://maps.app.goo.gl/GEyCJL2RWeGXBXcg7&sa=D&source=editors&ust=1716907751999759&usg=AOvVaw26xOtmMLhq94x55HxYK1kE)

* um prédio com a arquitetura muito diferente

* Região: [Prague Jewish Quarter](https://www.google.com/url?q=https://maps.app.goo.gl/Gh93YTqJ4ZVVbjCb9&sa=D&source=editors&ust=1716907752000346&usg=AOvVaw1CHNrdLQJ6kU-l_IqFqoKq)

* região com obras e prédios judaicos

* [Prague Jewish Quarter](https://www.google.com/url?q=https://maps.app.goo.gl/Gh93YTqJ4ZVVbjCb9&sa=D&source=editors&ust=1716907752000922&usg=AOvVaw0wlYW0sN9b474s0ukGtV3I) - se for nos horários certos ele faz um “show” com bonecos e tal, bem legal. 🙂
* Estátua com movimento: [Franz Kafka - Rotating Head by David Cerny](https://www.google.com/url?q=https://maps.app.goo.gl/EnA8sYth4p61EvMK7&sa=D&source=editors&ust=1716907752001481&usg=AOvVaw0qDMQNH8T9W17uvr9Z_qWx) - uma cabeça, que gira. 🤷
* [Klementinum](https://www.google.com/url?q=https://maps.app.goo.gl/dWaBgd1UQwn7sQzf9&sa=D&source=editors&ust=1716907752001942&usg=AOvVaw1ZTE2VnaCp15eDByvft2RR) - museus/bibliotecas. Tem umas coisas bem bonitas para ver, mas confesso que já não lembro muito
* [Prague Castle](https://www.google.com/url?q=https://maps.app.goo.gl/k3LCTxTwwax8izR27&sa=D&source=editors&ust=1716907752002333&usg=AOvVaw1OLRGTveTq1QiZxCMtRZvM) - muito lindo, com bela vista do rio. Dentro dos muros do castelo tem uma igreja que tb é bem linda (não entramos na igreja).
* [Letna Park](https://www.google.com/url?q=https://maps.app.goo.gl/zN4Kawk2aj7yzXo1A&sa=D&source=editors&ust=1716907752002746&usg=AOvVaw1Lj8pCK46fZWl6t9KjweSV) - parque com vista para o rio
* [Charles Bridge](https://www.google.com/url?q=https://maps.app.goo.gl/MMKTVLAESfdsvMVBA&sa=D&source=editors&ust=1716907752003185&usg=AOvVaw2wNg9qrGLDXLt88z_5ryAU) - ponte com obras/prédios dos dois lados com vista para o castelo e rio
* [Hemingway Bar](https://www.google.com/url?q=https://maps.app.goo.gl/VRyTPhfwosqADt6w7&sa=D&source=editors&ust=1716907752003563&usg=AOvVaw0qXKJpN7tNFwPiO8pmVF37) - Mixologista. Bar para drinks (não tem cerveja lá, e comida é só aperitivos/petiscos), mas ótimo para um fim de noite. É bem pequeninho (mas acho que dá para fazer reserva online no site). Eu voltaria lá para tomar uns Smashed Basil (manjericão, suco de limão e gin) 🍸
