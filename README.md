![gravatar](https://www.gravatar.com/avatar/2c41115521424eb593d93c44b92ced0c?s=240#center) <!-- markdownlint-disable MD041 -->

# Hi There! :wave: <!-- markdownlint-disable MD026 -->

## Who is Kassio Borges? <!-- markdownlint-disable MD026 -->

Such a philosophical question, but I'll keep it simple.

My name is Kassio Borges, and I'm a Software Engineer at Gitlab who likes coding, communication,
leadership podcasts.

### Some extra information

- **Current job title** Senior Backend Engineer - Create:Editor, Gitlab
- **Timezone** I travel a lot, but my default timezone is GMT
- **Pronouns** he/him/his

| [gitlab](https://gitlab.com/kassio) |
  [github](https://github.com/kassio) |
  [linkedin](https://linkedin.com/in/kassioborges) |
  [blog](https://kassioborges.com) |

### About me

Software developer since 2008, worked with some different technologies over the years. Main focus
over the years was Ruby, Rails and the related tech stack. But also worked with Elixir and Go.

Ruby on Rails contributor since 2013.

I like coding, music, movies, traveling and experiencing different food.

### My work style

- Process oriented (most of the time)
- I like to understand the problem before working on the solution
- I'll try my best to deliver the most iterative solution I can find
- Like to work [non-linear working hours](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/)
  - I travel a lot, so it might be hard sometimes to keep linear working hours. Besides that, I try
  to work **concurrently** on 2 or 3 issues at a time, more is too much for me.
    - **concurrently, not in parallel**: which means that when I have to wait for a code review, a
      question to be answered, or something like that, I can do a bit of work on the _other_ issue.
- I like to learn the best way to solve the right problem
  - I'd rather spend some time to understand and solve the right problem than solve quickly the wrong problem.
  - I'm always trying to improve my tools to work better
  - "If I only had an hour to chop down a tree, I would spend the first 45 minutes sharpening my axe." – Abraham Lincoln.

### My communication style

I'm an extrovert and social being. I like to do social calls, coffee chats, pair programming, etc.
But I think it's really important to send the right message through the right channels and to the
right audience. Therefore, for my work I prefer async and public channels to enable collaboration
through transparency.

### My working tools :tools:

#### Hardware

- :apple: Apple:
  - iPhone Pro
  - iPad (second monitor when travelling)
  - MacBook Pro
  - Airpods Pro
  - Apple external trackpad
- :desktop: Monitor: Dell U2720Q
- :keyboard: Keyboards (to avoid shoulders pain use split keyboards):
  - Currently on a [Levinson](https://keeb.io/products/levinson-lets-split-w-led-backlight)
    - Kailh Black - low profile
  - Waiting for a [wireless FeelixKeeb](https://shop.beekeeb.com/product/wireless-felix-keeb/)
    - Kailh Brown - low profile

#### Software

- Shell: zsh
- Terminal: Wezterm :moon:
  - It's fast and the configuration are lua files, which is nice since I already use lua for neovim.
- Editor/IDE: neovim :moon:
  - I started using vim back in 2005. I tried many editors/IDEs over the time, but once I got good
    experience with vim was hard to change by other tools.
  - When neovim came out in 2015 I had a lot of vim experience already, and changing wasn't too
    hard. For a while I kept my configuration interchangeable between vim/neovim, but after some
    time I just rewrote it in lua for better performance and readability.
    No regrets. :smiley:
- Browser: Safari
  - Less power consumption and forces me to be more focused. Without profiles I have to be more
    conscious about the tabs I'm opening/closing.
- Calendar:
  - Google Calendar
  - Macos menu: itsycal (synced via apple calendar)
- Music:
  - Spotify
  - Youtube music (sometimes to see video clips)
- Notes:
  - Apple Notes (personal and/or ephemeral notes)
  - GitLab Wiki/issues/epics/etc (transparency is important)
- Tasks:
  - Apple reminders
  - Google tasks (for things I want to see in the Calendar)
- Automation:
  - Alfred:
    - Browser bookmark search
    - Clipboard history
    - Calculator
    - MacOS snippet manager
    - Gitlab Helpers
    - File finder
  - Global Shortcuts: keyboard maestro
    - I like to be able to create custom global shortcuts and I use keyboard maestro for that. Some
      examples:
      - disable single cmd+q on Safari, instead use double cmd+q to close it
      - disable cmd+q and cmd+w on Kitty
      - window position management, ctrl+shift+1 to move a window to the half left for example
      - Lock the screen & sleep screen
